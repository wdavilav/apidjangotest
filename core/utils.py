from config import wsgi
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from config import settings


def insert_users():
    # insert admin
    user = User(first_name='Admin', last_name='Admin', username='admin', email='admin@gmail.com', password='admni123')
    user.save()
    token = Token.objects.get_or_create(user=user)
    print(f'Usuario {user.id} registrado correctamente')
    print(f'Token {token[0]}')
    # insert users for example api
    with open(f'{settings.BASE_DIR}/resources/users.cvs', 'r') as file:
        for line in file.readlines():
            row = line.strip().split(',')
            names = row[0].split(' ')
            user = User()
            user.username = row[-2]
            user.first_name = f'{names[0]} {names[1]}'
            user.last_name = f'{names[-2]} {names[-1]}'
            user.email = row[-1]
            user.set_password(user.username)
            user.save()
            print(f'Usuario {user.get_full_name()} registrado correctamente user/password={user.username}')


insert_users()
