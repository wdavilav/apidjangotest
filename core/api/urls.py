from django.urls import path
from core.api.views import *

urlpatterns = [
    path('login/', LoginAPIView.as_view(), name='api_login'),
]
