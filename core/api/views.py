from django.contrib.auth import authenticate
from django.forms import model_to_dict
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView


class LoginAPIView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        response = {'resp': False, 'msg': 'El usuario no existe'}
        try:
            username = request.data.get('username')
            password = request.data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                response = {'resp': True, 'user': model_to_dict(user, exclude=['last_login', 'email_reset_token', 'password', 'user_permissions', 'groups', 'date_joined'])}
        except Exception as e:
            response = {'resp': False, 'msg': str(e)}
        return Response(response)
